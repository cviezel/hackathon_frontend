import React, { Component } from 'react';
import logo from './favelogo2.png';
import './App.css';
import {Table} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from "react-bootstrap";
import { Container, Row, Col } from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'BUY',
      ticker: '',
      qty: '',
      price: '',
      trades: [],
      portfolios:[],
      funds: '',
      live_prices: [],
      new_live_prices: {},
      total_portfolio_value: 0,
      graph: '',
      chart: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleOption = this.handleOption.bind(this)
  }

  //Sends a POST request to the Trade service
  sendTradePOSTRequest() {
    fetch('http://localhost:8080/trade', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        type: this.state.value,
        ticker: this.state.ticker,
        quantity: this.state.qty,
        requestedPrice: this.state.price
      })
    })
  }
  
  //Acquires the live price of the given ticker and then sends a POST request to the trade service and clears the input fields
  submitTrade(){
    const url = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=' + this.state.ticker + '&num_days=1'
    fetch(url)
    .then(res => res.json())
    .then((data) => {
      this.setState({ live_prices: data })
      this.setState({price: this.state.live_prices.price_data[0][1]})
      console.log(this.state.live_prices)
      this.sendTradePOSTRequest()
      this.handleReset()
      this.refreshImage()
    })
    .catch(e => {
      console.log(e);
    });
  }

  //Gets the live price of every stock in the portfolio
  getLivePrice(ticker) {
    console.log('run')
    if(ticker in this.state.new_live_prices) {
      return this.state.new_live_prices[ticker]
    }
    const url = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed?ticker=' + ticker + '&num_days=1'
    fetch(url)
    .then(res => res.json())
    .then((data) => {
      this.state.new_live_prices[ticker] = data.price_data[0][1]
    })
    .catch(e => {
      console.log(e);
    });
  }

  //Runs when the trade form is submitted and actually submits the trade
  handleSubmit(e) {
    e.preventDefault()
    this.submitTrade()
  }

  //Changes the state of the option from Buy to Sell
  handleOption(e) {
    this.setState({value: e.target.value});
  }

  //Changes the state of the textboxes when input is written
  handleChange(e) {
    const target = e.target;
    const name = target.name;
    this.setState({
      [name]: target.value
    });
  }

  //Resets the text values on the inputs
  handleReset = () => {
    Array.from(document.querySelectorAll("input")).forEach(
      input => (input.value = "")
    );
    this.setState({
      itemvalues: [{}]
    });
  };

  //Runs a GET request on the trade database and displays the new results
  refreshHistory = () => {
    fetch('http://localhost:8080/trade')
    .then(res => res.json())
    .then((data) => {
      this.setState({ trades: data })
    })
  }

  //Runs a GET request on the funds database and displays the new results
  getFunds = () => {
    fetch('http://localhost:8087/funds')
    .then(res => res.json())
    .then((data) => {
      this.setState({ funds: data })
    })
  }

  //POSTS the portfolio received from the portfolio database to the graphing API
  postPortfolioToGraph(payload) {
    fetch('http://localhost:5000/buildPortfolio', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: payload
    })
  }

  //Generates the total value of your portfolio
  getPortfolioValue() {
    let sum = 0;
    for (var i in this.state.portfolios) {
      if(this.state.portfolios[i].id in this.state.new_live_prices) {
        sum += this.state.new_live_prices[this.state.portfolios[i].id] * this.state.portfolios[i].quantity;
      }
    }
    return sum;
  }

  //Runs a GET request on the portfolio database and displays the new results
  refreshPortfolio = () => {
    fetch('http://localhost:8087/portfolio')
    .then((res) => {
      return res.json()
    })
    .then((data) => {
      this.setState({ portfolios: data })
      this.getFunds()
      //update graph API POST
      this.postPortfolioToGraph(JSON.stringify(data))
      this.setState({total_portfolio_value: this.getPortfolioValue()})
    })
  }

  //Runs a GET request on the graph API to display a new image
  refreshImage = () => {
    let str = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    let imgUrlGraph = "http://127.0.0.1:5000/graph?" + str
    this.setState({graph: <img class="graphs" src={imgUrlGraph} alt="graph"></img>})
    let imgUrlChart = "http://127.0.0.1:5000/chart?" + str
    this.setState({chart: <img class="graphs" src={imgUrlChart} alt="graph"></img>})
  }

  componentDidMount() {
    this.getFunds()
    this.refreshImage()
    this.refreshHistory()
    this.refreshPortfolio()
    setInterval(this.refreshHistory, 5000); // runs every 5 seconds.
    setInterval(this.refreshPortfolio, 10000) // runs every 10 seconds.
    setInterval(this.refreshImage, 10000); // runs every 10 seconds.
  }

  render() {

    let trades = this.state.trades.map((trade) => {
      return(
        <tr key={trade.id}>
          <td>{trade.dateCreated}</td>
          <td>{trade.ticker}</td>
          <td>{trade.quantity}</td>
          <td>{trade.requestedPrice}</td>
          <td>{trade.type}</td>
          <td>{trade.state}</td>
          <td>{trade.quantity*trade.requestedPrice}</td>
        </tr>
      )
    }); 

    //new portfolio code
    let portfolios = this.state.portfolios.map((port) => {
      if(port.id in this.state.new_live_prices) {
      } else {
        this.getLivePrice(port.id)
      }
      return(
        this.state && port.id in this.state.new_live_prices &&
        <tr key={port.id}>
          <td>{port.id}</td>
          <td>{port.quantity}</td>
          <td>{this.state.new_live_prices[port.id]}</td>
          <td>{this.state.new_live_prices[port.id]*port.quantity}</td>
        </tr>
      )
    });
    
    return (
      <div>
        <header className="App-header">
          <body className ="common_background">
            <img src={logo} alt="logo here" width="275px" height="150px"/>

            <h1>Welcome to our Trade Service!</h1>

              <Row>
                <Col md="auto"><h3>Available Funds: {this.state.funds.funds}</h3></Col>
                <Col md="auto"><h3>Total Portfolio Value: {this.state.total_portfolio_value}</h3></Col>
              </Row>
              
              
              <Form onSubmit={this.handleSubmit}>
                <p1>What would you like to do today?</p1>
                <p style={{margin: '28px'}}>
                  <label>Trade Type&nbsp;</label>
                      <select value={this.state.value} onChange={this.handleOption} >
                        <option value = "BUY">Buy</option>
                        <option value = "SELL">Sell</option>
                      </select>&emsp;

                  <label for="ticker">Ticker Symbol:&nbsp;</label>
                  <input type="text" name="ticker" onChange={this.handleChange} />&emsp;

                  <label for="qty">Quantity:&nbsp;</label>
                  <input type="text" name="qty" onChange={this.handleChange} />

                  {/* <p><input class ="btn" type="submit" value="Submit"></input></p> */}
                  <p><Button variant="light" type="submit" value="Submit">Submit</Button></p>
                </p>
              </Form>

            <h1>Trade History</h1>

            <div>
              <center>
                <Table className="trade">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Ticker</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Type</th>
                      <th>State</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    {trades}
                  </tbody>
                </Table> 
              </center>
              </div>
              <h1>Investment Portfolio</h1>
              <h3>Total Assets: {this.state.portfolios.length}</h3>
              <div class="grid-container">
                  <Table>
                    <thead>
                      <tr>
                        <th>Ticker</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      {portfolios}
                    </tbody>
                  </Table>
              </div>
              <div class="float-container"> 
                  <div class="float-child">
                    {this.state.graph}
                  </div>
                  <div class="float-child">{this.state.chart}
                  </div>
              </div>
          </body>
        </header>
      </div> 
    );
  }
}

export default App;
